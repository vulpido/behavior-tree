#include "nodes.hpp"

using namespace BT;

void Node::enter() {
	on_enter();
}

Node::State Node::exit(State state) {
	if (state != State::Running) {
		on_exit();
	}

	return state;
}

void Node::on_enter() {}

void Node::on_exit() {}

/// Decorators

void Decorator::set_child(std::shared_ptr<Node> child) {
	this->child = child;
}

std::shared_ptr<Node> Decorator::get_child() const {
	return child;
}

void Decorator::on_enter() {
	if (child) {
		child->enter();
	}
}

Node::State Inverter::tick(double delta) {
	State result = State::Failure;

	if (child) {
		State state = child->tick(delta);

		switch (state) {
			case State::Failure: result = State::Success; break;
			case State::Success: result = State::Failure; break;
			case State::Running: result = State::Running; break;
		}
	}

	return exit(result);
}

Node::State Succeeder::tick(double delta) {
	State result = State::Success;

	if (child) {
		State state = child->tick(delta);

		if (state == State::Running) {
			result = State::Running;
		}
	}

	return result;
}

bool Repeater::has_count() const {
	return count > 0;
}

void Repeater::set_count(unsigned int count) {
	if (this->count != count) {
		this->count = count;
	}
}

unsigned int Repeater::get_count() const {
	return count;
}

void Repeater::on_enter() {
	if (has_count()) {
		counter = count;
	}

	Decorator::on_enter();
}

Node::State Repeater::tick(double delta) {
	bool repeat  = true;
	State state  = child->tick(delta);
	State result = State::Running;

	if (has_count()) {
		counter--;

		if (counter <= 0) {
			repeat = false;
			result = State::Success;
		}
	}

	if (state != State::Running && repeat) {
		child->enter();
	}

	return exit(result);
}

void RepeatUntilFail::on_enter() {
	once = false;

	Decorator::on_enter();
}

Node::State RepeatUntilFail::tick(double delta) {
	State result = State::Running;
	State state  = child->tick(delta);

	switch (state) {
		case State::Success:
			result = State::Running;
			once   = true;

			child->enter();
			break;
		
		case State::Failure:
			if (once) {
				result = State::Success;
			} else {
				result = State::Failure;
			}
			break;
		default:
			break;
	}

	return exit(result);
}

/// Composites

void Composite::add_child(std::shared_ptr<Node> child) {
	children.push_back(child);
}

void Composite::remove_child(std::shared_ptr<Node> child) {
	auto it = children.begin();
	while (it != children.end() && *it != child) {
		it++;
	}
	
	if (it != children.end()) {
		children.erase(it);
	}
}

const std::vector<std::shared_ptr<Node>>& Composite::get_children() const {
	return children;
}

void Composite::on_enter() {
	current_index = 0;

	if (current_index < children.size()) {
		children[current_index]->enter();
	}
}

Node::State Sequence::tick(double delta) {
	State result = State::Running;
	State state  = State::Failure;

	if (current_index < children.size()) {
		state = children[current_index]->tick(delta);
	}

	if (state == State::Success) {
		current_index++;

		if (current_index < children.size()) {
			result = State::Running;
			children[current_index]->enter();
		} else {
			result = State::Success;
		}
	} else if (state == State::Failure) {
		result = State::Failure;
	}

	return exit(result);
}

Node::State Selector::tick(double delta) {
	State result = State::Running;
	State state  = State::Failure;

	if (current_index < children.size()) {
		state = children[current_index]->tick(delta);
	}

	if (state == State::Success) {
		result = State::Success;
	} else if (state == State::Failure) {
		current_index++;
		
		if (current_index < children.size()) {
			result = State::Running;
			children[current_index]->enter();
		} else {
			result = State::Failure;
		}
	}

	return exit(result);
}
