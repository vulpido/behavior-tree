#pragma once

#include <memory>
#include <vector>

namespace BT {

class Node {
public:
	enum class State {
		Failure,
		Success,
		Running
	};

	void enter();
	virtual State tick(double delta) = 0;

protected:
	virtual void on_enter();
	virtual void on_exit();

	State exit(State);
};

/// Decorators

class Decorator : public Node {
protected:
	std::shared_ptr<Node> child;

	void on_enter() override;

public:
	void set_child(std::shared_ptr<Node>);
	std::shared_ptr<Node> get_child() const;
};

class Inverter : public Decorator {
public:
	State tick(double delta) override;
};

class Succeeder : public Decorator {
public:
	State tick(double delta) override;
};

class Repeater : public Decorator {
protected:
	unsigned int count   = 0;
	unsigned int counter = 0;

	void on_enter() override;

public:
	bool has_count() const;

	void set_count(unsigned int);
	unsigned int get_count() const;

	State tick(double delta) override;
};

class RepeatUntilFail : public Decorator {
protected:
	bool once;

	void on_enter() override;

public:
	State tick(double delta) override;
};

/// Composites

class Composite : public Node {
protected:
	std::vector<std::shared_ptr<Node>> children;
	unsigned int current_index;

	void on_enter() override;

public:
	void add_child(std::shared_ptr<Node>);
	void remove_child(std::shared_ptr<Node>);

	const std::vector<std::shared_ptr<Node>>& get_children() const;
};

class Sequence : public Composite {
public:
	State tick(double delta) override;
};

class Selector : public Composite {
public:
	State tick(double delta) override;
};

}
